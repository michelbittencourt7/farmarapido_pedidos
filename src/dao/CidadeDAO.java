    /*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import dto.CidadeDTO;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import jdbc.ConexaoJDBC;

/**
 *
 * @author miche
 */
public class CidadeDAO {
    private static final String SQL_INSERT = "INSERT INTO cidade (nome, estado, estado_idestado) VALUES (?,?,?)";
    
    public void incluir(CidadeDTO obj) throws ClassNotFoundException {
        try {
            Connection conn = new ConexaoJDBC().getInstance().getConnection();
            PreparedStatement ps = conn.prepareStatement(SQL_INSERT);
            ps.setString(1, obj.getNome());
            ps.setString(2, obj.getEstado());
            ps.setString(3, String.valueOf(obj.getIdCidade()));
            ps.execute();
        } catch (SQLException ex){
            Logger.getLogger(CidadeDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    
}
