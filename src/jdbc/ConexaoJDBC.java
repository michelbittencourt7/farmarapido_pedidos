/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jdbc;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 *
 * @author miche
 */
public class ConexaoJDBC {
    private static String servidor = "localhost:3306/";
    private static String usuario = "root";
    private static String senha = "";
    private static String banco = "mydb";
    
    private static String driverTipo = "jdbc:mysql://";
    private static String driver = "com.mysql.jdbc.Driver";
    
    private static ConexaoJDBC conexaoJDBC;
    
    public static ConexaoJDBC getInstance() {
        if(conexaoJDBC == null) {
            conexaoJDBC = new ConexaoJDBC();
        }
        return conexaoJDBC;
    }
    
    public static Connection getConnection() throws ClassNotFoundException, SQLException {
        Class.forName(driver);
        return DriverManager.getConnection(driverTipo + servidor + banco, usuario, senha);
    }
    
    
    
    
}
